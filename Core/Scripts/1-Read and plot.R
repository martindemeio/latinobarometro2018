initial=0
if (initial==1){
  install.packages('data.table')
  install.packages('ggplot2') 
  install.packages('tidyverse') 
  install.packages('foreign')
  install.packages("readstata13")
  install.packages("car")
  install.packages("extrafont")
}



library(data.table)
library(ggplot2) 
library(tidyverse) 
library(foreign)
library(readstata13)
library(car)
library(extrafont)


theme_elegante <- function(base_size = 10,
                           base_family = "Raleway"
){
  color.background = "#FFFFFF" # Chart Background
  color.grid.major = "#D9D9D9" # Chart Gridlines
  color.axis.text = "#666666" # 
  color.axis.title = "#666666" # 
  color.title = "#666666"
  color.subtitle = "#666666"
  strip.background.color = '#9999CC'
  
  ret <-
    theme_bw(base_size=base_size) +
    
    # Set the entire chart region to a light gray color
    theme(panel.background=element_rect(fill=color.background, color=color.background)) +
    theme(plot.background=element_rect(fill=color.background, color=color.background)) +
    theme(panel.border=element_rect(color=color.background)) +
    
    # Format the grid
    theme(panel.grid.major=element_line(color=color.grid.major,size=.55, linetype="dotted")) +
    theme(panel.grid.minor=element_line(color=color.grid.major,size=.55, linetype="dotted")) +
    theme(axis.ticks=element_blank()) +
    
    # Format the legend, but hide by default
    theme(legend.position="none") +
    theme(legend.background = element_rect(fill=color.background)) +
    theme(legend.text = element_text(size=base_size-3,color=color.axis.title, family = base_family)) +
    
    theme(strip.text.x = element_text(size=base_size,color=color.background, family = base_family)) +
    theme(strip.text.y = element_text(size=base_size,color=color.background, family = base_family)) +
    #theme(strip.background = element_rect(fill=strip.background.color, linetype="blank")) +
    theme(strip.background = element_rect(fill = "grey70", colour = NA)) +
    # theme(panel.border= element_rect(fill = NA, colour = "grey70", size = rel(1)))+
    # Set title and axis labels, and format these and tick marks
    theme(plot.title=element_text(color=color.title, 
                                  size=20, 
                                  vjust=1.25, 
                                  family=base_family, 
                                  hjust = 0.5
    )) +
    
    theme(plot.subtitle=element_text(color=color.subtitle, size=base_size+2, family = base_family,  hjust = 0.5))  +
    
    theme(axis.text.x=element_text(size=base_size,color=color.axis.text, family = base_family)) +
    theme(axis.text.y=element_text(size=base_size,color=color.axis.text, family = base_family)) +
    theme(text=element_text(size=base_size, color=color.axis.text, family = base_family)) +
    
    theme(axis.title.x=element_text(size=base_size+2,color=color.axis.title, vjust=0, family = base_family)) +
    theme(axis.title.y=element_text(size=base_size+2,color=color.axis.title, vjust=1.25, family = base_family)) +
    theme(plot.caption=element_text(size=base_size-2,color=color.axis.title, vjust=1.25, family = base_family)) +
    
    # Legend  
    theme(legend.text=element_text(size=base_size,color=color.axis.text, family = base_family)) +
    theme(legend.title=element_text(size=base_size,color=color.axis.text, family = base_family)) +
    theme(legend.key=element_rect(colour = color.background, fill = color.background)) +
    theme(legend.position="bottom", 
          legend.box = "horizontal", 
          legend.title = element_blank(),
          legend.key.width = unit(.75, "cm"),
          legend.key.height = unit(.75, "cm"),
          legend.spacing.x = unit(.25, 'cm'),
          legend.spacing.y = unit(.25, 'cm'),
          legend.margin = margin(t=0, r=0, b=0, l=0, unit="cm")) +
    
    # Plot margins
    theme(plot.margin = unit(c(.5, .5, .5, .5), "cm"))
  
  ret
}
data_core_A<-read.dta13("Data/Latinobarometro_2018_Eng_Stata_v20190303.dta",convert.factors = F)

#### improved method START
data_core_B<-read.dta13("Data/Latinobarometro_2018_Eng_Stata_v20190303.dta",convert.factors = T)


dataframe2<- as.data.frame(data_core_B)    

varlist<-names(dataframe2)

varlist<-gsub('\\.', '_', varlist)
names(dataframe2)<-varlist
write.dta(dataframe2,file="Latinobarometro2018_Fixed.dta",version = 7L,
          convert.dates = TRUE, tz = "GMT",
          convert.factors = c())

data_core_B<-data_core_A
#### improved method END



data_core<-data_core_A[which(data_core_A$S1!=-1 & data_core_A$S1!=-2),]#clase
data_core<-data_core[which(data_core$S10!=96 | data_core$S10!=97 | data_core$S10!=98),]#educacion
#data_core<-data_core[data_core$S9!=96 & data_core$S9!=97 & data_core$S9!=98,]#educacion years


# data_core<-data_core[which(  data_core$IDENPA==152 |
#                              data_core$IDENPA==188 |
#                              data_core$IDENPA==214 |
#                              data_core$IDENPA==858),]
# 
# data_core$IDENPA<-recode(data_core$IDENPA,'"152"="Chile";
#                                             "188"="Costa Rica";
#                                              "214"="Rep. Dominicana";
#                                            "858"="Uruguay"')


data_core<-data_core[which(  data_core$S10==1  |# Withot edu
                             data_core$S10==2  |# Primary until the 13
                             data_core$S10==3  |
                             data_core$S10==4  |
                             data_core$S10==5  |
                             data_core$S10==6  |
                             data_core$S10==7  |
                             data_core$S10==8  |
                             data_core$S10==9  |
                             data_core$S10==10 |
                             data_core$S10==11 |
                             data_core$S10==12 |
                             data_core$S10==13 |
                             data_core$S10==14 |# Incomplete university
                             data_core$S10==15 |# Complete university
                             data_core$S10==16 |# H.Sch
                             data_core$S10==17),]# VET

names(data_core)[names(data_core) == 'S1'] <- 'SocialClass'
names(data_core)[names(data_core) == 'S10'] <-  'Education'

data_core$Education<-as.character(data_core$Education)
data_core$Education<-recode(data_core$Education,'"1"="No Edu.";
                                                "2"="H.School or less";
                                                "3"="H.School or less";
                                                "4"="H.School or less";
                                                "5"="H.School or less";
                                                "6"="H.School or less";
                                                "7"="H.School or less";
                                                "8"="H.School or less";
                                                "9"="H.School or less";
                                                "10"="H.School or less";
                                                "11"="H.School or less";
                                                "12"="H.School or less";
                                                "13"="H.School or less";
                                                "14"="In Univ.";
                                                "15"="Univ. Degree";
                                                "16"="H.School or less";
                                                "17"="Vocational"')

data_core$SocialClass<-recode(data_core$SocialClass,'"1"="1";"2"="1";"4"="5";"5"="5"')
data_core$SocialClass<-recode(data_core$SocialClass,'"1"="Low";"3"="Middle";"5"="High"')

EduFactors<-c("No Edu.","H.School or less","H.School",
              "Vocational","In Univ.",
              "Univ. Degree")

SoCClasFactor<-c(c("Low","Middle","High"))
data_core$Education<-factor(data_core$Education, levels=EduFactors)
data_core$SocialClass<-factor(data_core$SocialClass, levels=SoCClasFactor)

fwrite(data_core,"Datapost/PostR_Latinobarometro_2018.csv")

data_core             <-data_core[data_core$P34NI.AA>0,]




#filter
data_core<-data_core[which(  data_core$Education!="Vocational"),]



Led_Know             <-data_core[data_core$P34NI.AA>0,]
SolarPanel_Know      <-data_core[data_core$P34NI.AC>0,]
ElectricCar_Know     <-data_core[data_core$P34NI.AB>0,]
ElectricityApps_Know <-data_core[data_core$P34NI.AD>0,]

Led_Uses              <-data_core[data_core$P34NI.BA>0,]#LED
SolarPanel_Uses       <-data_core[data_core$P34NI.BC>0,]#Solar
ElectricCar_Uses      <-data_core[data_core$P34NI.BB>0,]#VElectrico
ElectricityApps_Uses  <-data_core[data_core$P34NI.BD>0,]#Apps

Led_WouldLike             <-data_core[data_core$P34NI.CA>0,]
SolarPanel_WouldLike      <-data_core[data_core$P34NI.CC>0,]
ElectricCar_WouldLike     <-data_core[data_core$P34NI.CB>0,]
ElectricityApps_WouldLike <-data_core[data_core$P34NI.CD>0,]

AwarenesFactor<-c("Not Knows","Knows","Uses")


SolarPanel_Know      <-data_core[data_core$P34NI.AC>0,]
SolarPanel_Know      <-SolarPanel_Know[SolarPanel_Know$P34NI.BC>0,]
SolarPanel_Know$Not_knows<-ifelse(SolarPanel_Know$P34NI.AC==1,"Knows","Not Knows")
SolarPanel_Know$Ownership<-ifelse(SolarPanel_Know$P34NI.BC==1,"Uses","Knows")
SolarPanel_Know$Ownership[SolarPanel_Know$Not_knows=="Not Knows"]<-"Not Knows"
SolarPanel_Know$Ownership<-as.factor(SolarPanel_Know$Ownership)
SolarPanel_Know$Ownership<-factor(SolarPanel_Know$Ownership, levels=AwarenesFactor)


ElectricCar_Know     <-data_core[data_core$P34NI.AB>0,]
ElectricCar_Know     <-ElectricCar_Know[ElectricCar_Know$P34NI.BB>0,]
ElectricCar_Know$Not_knows<-ifelse(ElectricCar_Know$P34NI.AB==1,"Knows","Not Knows")
ElectricCar_Know$Ownership<-ifelse(ElectricCar_Know$P34NI.BB==1,"Uses","Knows")
ElectricCar_Know$Ownership[ElectricCar_Know$Not_knows=="Not Knows"]<-"Not Knows"
ElectricCar_Know$Ownership<-as.factor(ElectricCar_Know$Ownership)
ElectricCar_Know$Ownership<-factor(ElectricCar_Know$Ownership, levels=AwarenesFactor)
table(ElectricCar_Know$Ownership)


fwrite(Lati12018,"Datapost/Solar_Stata_PostR_Latinobarometro_2018.csv")
fwrite(ElectricCar_Know,"Datapost/EV_Stata_PostR_Latinobarometro_2018.csv")

Plot_dataset<-SolarPanel_Know


ggplot(Plot_dataset,aes(x=Education,y=SocialClass,alpha=0.9,col=Ownership,shape=Ownership))+
  geom_jitter(position = "jitter",size=5)+
  geom_smooth(method = "gam",color="black",se = F)+
  xlab("Education")+ylab("Declared Income")+ggtitle("Who is aware and uses Solar panels?")+
  facet_wrap(~IDENPA)+
  guides(alpha=FALSE)+
  labs(color  = "Awareness", shape = "Awareness")+
  scale_color_manual(values=c("Not Knows"="black","Knows"="brown2", "Uses"="green3"))+
  scale_shape_manual(values=c("Not Knows"=21,"Knows"=20, "Uses"=16))+
  theme_elegante()+theme(plot.title = element_text(hjust = 0.5), axis.text=element_text(size=30))+
  theme(panel.border= element_rect(fill = NA, colour = "grey70", size = rel(1))) 

ggsave("Solar Awareness.tiff", width=500, height=300, units="mm", dpi=300, compression = "lzw")


Plot_dataset<-ElectricCar_Know

ggplot(Plot_dataset,aes(x=Education,y=SocialClass,alpha=0.9,col=Ownership,shape=Ownership))+
  geom_jitter(position = "jitter",size=5)+
  geom_smooth(method = "gam",color="black",se = F)+
  xlab("Education")+ylab("Declared Income")+ggtitle("Who is aware and uses Electric Vehicles?")+
  guides(alpha=FALSE)+facet_wrap(~IDENPA)+
  labs(color  = "Awareness", shape = "Awareness")+
  scale_color_manual(values=c("Not Knows"="black","Knows"="brown2", "Uses"="green3"))+
  scale_shape_manual(values=c("Not Knows"=21,"Knows"=20, "Uses"=16))+
  theme_elegante()+theme(plot.title = element_text(hjust = 0.5), text = element_text(size=20))+
  theme(panel.border= element_rect(fill = NA, colour = "grey70", size = rel(1)))

ggsave("EV Awareness.tiff", width=500, height=300, units="mm", dpi=300, compression = "lzw")

# 
# Plot_dataset<-ElectricityApps_Know_Si
# 
# 
# ggplot(Plot_dataset,aes(x=Education,y=SocialClass,alpha=0.1,color=Ownership))+
#   geom_jitter(position = "jitter",color="purple4")+geom_smooth(method = "gam",color="black",se = F)+
#   xlab("Education")+ylab("Social Class")+ggtitle("Who is aware of Electricity apps")+
#   guides(alpha=FALSE)+facet_wrap(~IDENPA)+
#   theme_classic()+theme(plot.title = element_text(hjust = 0.5))
# 
# 
# 
# Plot_dataset<-Know_Si_mix
# 
# ggplot(Plot_dataset,aes(x=Education,y=SocialClass,alpha=0.1,color=Ownership))+
#   geom_jitter(position = "jitter",size=3)+geom_smooth(method = "gam",color="black",se = F)+
#   xlab("Education")+ylab("Social Class")+ggtitle("Who is aware of Solar panels and El.Vehicles")+
#   guides(alpha=FALSE)+facet_wrap(~IDENPA)+
#   theme_classic()+theme(plot.title = element_text(hjust = 0.5))
# 
# 
# 
# 
# 
