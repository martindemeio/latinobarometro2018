
*### 		For dealing diretcly with STATA file 		##

*##		 Marginal effects on Mlogit for Apps    ##

*# 					Presets 							#

clear
set more off
import delimited "C:\Users\jesuschu\Desktop\Grafico DIA Tech Ed\Core\DataPost\Solar_Stata_PostR_Latinobarometro_2018.csv", clear 

*#					 Setup 								#

*Numero pais
*drop if IDENPA== 
*social class
drop if s1  < 0 
*Education

gen  Education    = s10
drop if s10 < 0 
gen  SocialClass = s1

recode Education (2/13=2) (14=15) 
recode Education (1=1) (15=4) (16=3) (17=3)
recode SocialClass (2=1) (4=3) 
recode SocialClass (3=2) (5=3) 

label define Educationl    1 "No Edu" 2 "Incomplete Compulsory Education" 3 "H.School and Vocational" 4 "University"
label define SocialClassl 1 "Low"  2 "Middle" 3 "High"

label values Education Educationl  
label values SocialClass SocialClassl  

*saveold "C:\Users\jesuschu\Desktop\Grafico DIA Tech Ed\Core\DataPost\Stata Lat2018 post.dta", replace



*## IV Apps ##

drop if p34niad < 0
gen Awareness = p34niad
recode Awareness (2=0) (1=1) 

label define Awarenessl 1 "Knows" 2 "Not Knows" 
label values Awareness Awarenessl  


*# model mlogit #

logit Awareness i.Education i.SocialClass
margins, dydx(*)


*# Charts #
logit Awareness i.Education i.SocialClass  i.Education##i.SocialClass


margins  if SocialClass ==1, dydx(Education)
margins  if SocialClass ==3, dydx(Education)

logit Awareness i.Education i.SocialClass  i.Education##i.SocialClass
margins, dydx(*)
margins , dydx(Education) over(SocialClass )
marginsplot


outreg using SCoverEDU.rtf, marginal replace se