


*## Marginal effects on Mlogit for Solar Panels    ##*
*# presets #*

clear
set more off

*# setup #*

import delimited   "C:\Users\jesuschu\Desktop\Grafico DIA Tech Ed\Core\DataPost\Solar_Stata_PostR_Latinobarometro_2018.csv"
encode   education , gen(EdId)
encode   socialclass , gen(SCId)
encode   not_knows , gen(AwarenessId)

label list AwarenessId
label list EdId
label list SCId


*# model mlogit #*

mlogit AwarenessId i.EdId i.SCId
margins, dydx(*)













