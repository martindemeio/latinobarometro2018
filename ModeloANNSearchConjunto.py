# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal.
"""

import numpy as np
import pandas as pd
from keras.layers import Input, Dense, Dropout
from keras.models import Model
from keras.callbacks import EarlyStopping
from keras import regularizers
from keras import backend as K
import matplotlib.pyplot as plt
import tensorflow as tf
import random as rn
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_auc_score
from sklearn.metrics import classification_report
from sklearn.model_selection import RandomizedSearchCV
from keras.wrappers.scikit_learn import KerasRegressor

"Fijar semilla para reproductibilidad"
np.random.seed(27)
tf.set_random_seed(27)
rn.seed(27)
# Force TensorFlow to use single thread.
# Multiple threads are a potential source of non-reproducible results.
tf.keras.backend.clear_session()

session_conf = tf.ConfigProto(intra_op_parallelism_threads=1,
                              inter_op_parallelism_threads=1)
sess = tf.Session(graph=tf.get_default_graph(), config=session_conf)
K.set_session(sess)

Datos = pd.read_excel('DatosLimpios.xls', sheet_name='Sheet1')
Escala = MinMaxScaler()

Education = pd.get_dummies(Datos.iloc[:,6],prefix="Education")
Woman = Datos.iloc[:,7]
Income = pd.get_dummies(Datos.iloc[:,9],prefix="Income")
Country = pd.get_dummies(Datos.iloc[:,12],prefix="Country")
Age = pd.DataFrame(Escala.fit_transform(Datos.iloc[:,8].ravel().reshape(-1,1)))
LNT = Datos.iloc[:,11]
Abroad = Datos.iloc[:,10]
AwaPV = Datos.iloc[:,0]
AwaEV = Datos.iloc[:,1]
AwaHYB = Datos.iloc[:,2]
AwaLED = Datos.iloc[:,3]
AwaAPP = Datos.iloc[:,4]

Inputs = pd.concat([Education.iloc[:,1:], Woman, Income.iloc[:,1:], LNT, Abroad, Country.iloc[:,1:], Age] , axis=1)

Outputs = pd.concat([AwaPV, AwaEV, AwaHYB, AwaLED, AwaAPP], axis=1)

InputTrain, InputTest, OutputTrain, OutputTest = train_test_split(Inputs, Outputs, test_size = 0.3, random_state=42)

"Parametros modelo"
Epocas = 20
CValidation = 0.20

Early = EarlyStopping(monitor='val_loss', patience=5, restore_best_weights=True)

def create_model(hidden1,
                 hidden2,
                 activation_fn1,
                 activation_fn2):

    Features = Input(shape=(InputTrain.shape[1],))
    Oculta1 = Dense(hidden1, activation=activation_fn1)(Features)
    Oculta2 = Dense(hidden2, activation=activation_fn2)(Oculta1)
    Outcomes = Dense(5)(Oculta2)

    Modelo = Model(inputs=Features, outputs=Outcomes)
    Modelo.compile(optimizer='adam', loss='mse')
    return Modelo

hidden1 = [20, 50, 70, 100]
hidden2 = [20, 50, 70, 100]
activation_fn1 = ['relu', 'tanh']
activation_fn2 = ['relu', 'tanh']

param_grid = dict(hidden1= hidden1, 
                  hidden2= hidden2,
                  activation_fn1 = activation_fn1,
                  activation_fn2 = activation_fn2)

model = KerasRegressor(build_fn=create_model) 

Modelo = RandomizedSearchCV(estimator=model, 
                    param_distributions=param_grid, 
                    cv=5,
                    verbose=True)

Historia = Modelo.fit(InputTrain, OutputTrain, epochs=Epocas, validation_split=CValidation, shuffle=True)

print(Historia.best_params_)