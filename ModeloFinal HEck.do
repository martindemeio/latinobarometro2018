set more off
clear
cls

*Carga de datos

insheet using "C:\Users\Martin\Documents\Posdoc\Latinobarometro2018\Datos.csv"
drop if s10 < 0 
drop if p34niaa < 0
drop if p34niab < 0
drop if p34niac < 0
drop if p34niad < 0
drop if p34niae < 0
drop if p34niba < 0 & idenpa != 218
drop if s1  < 0
drop if edad < 0
drop if sexo < 0
drop if p38ni < 0
drop if p62nf < 0
drop if p34niaa == 2 & p34niba ==1
drop if s21b < 0
drop if s13c < 0

*Variables dependientes
gen AwarenessPV = p34niac
gen AwarenessEV = p34niab
gen AwarenessHYB = p34niae
gen AwarenessLED = p34niaa
gen AwarenessAPP = p34niad

gen AdoptionLED = p34niba

*variables descriptivas del individuo, incluye Age y sexo
gen Woman = sexo
gen Age = edad
gen Country = idenpa

*Educacion: 5 categorias (1 baja escolaridad - alta escolaridad)
gen Education = s10

*Viajo al exterior alguna vez

gen Abroad = s13c

*Ingreso: 5 categorias (5 muy bajo - 1 muy alto)
gen SocialClass = s1

*Llevo/a adelante medidas de ahorro energetico 
gen Saving = p38ni

*Amigabilidad con nuevas tecnologias: cree que los niños deben aprender sobre nuevas tecnologias?
gen Friendliness = p62nf

*Tiene casa propia
gen Ownership = s21b

*Percepcion del individuo respecto a las 2 principales fuentes de energia consumidas por su pais: NonRenewable o no NonRenewable
gen NonRenewable = 1 if (p29nid==1) | (p29nie==1) | (p29nif==1)
replace NonRenewable = 0 if NonRenewable ==.

recode AwarenessPV (2=0)
recode AwarenessEV (2=0)
recode AwarenessHYB (2=0)
recode AwarenessLED (2=0)
recode AwarenessAPP (2=0)

recode AdoptionLED (2=0)

recode Woman (2=1) (1=0) 
recode Education (2/7=2) (8/13=3) (14/17=4) 
recode Saving (1=0)
recode Saving (2/4=1)
recode Friendliness (2=1)
recode Friendliness (3/4=0)
recode Ownership (2=0)
recode Abroad (1=0)
recode Abroad (2/4=1)

label define Educationl    1 "No Edu" 2 "Edu <= 7" 3 "Edu <=13" 4 "Edu > 14"
label define Awarenessl 1 "Knows" 0 "Not Knows"
label define Adoptionl 1 "Yes" 0 "No"

label values Education Educationl  
label values AwarenessPV Awarenessl
label values AwarenessEV Awarenessl
label values AwarenessHYB Awarenessl
label values AwarenessAPP Awarenessl
label values AwarenessLED Awarenessl
label values AdoptionLED Adoptionl
label values Saving Adoptionl
label values Friendliness Adoptionl
label values Ownership Adoptionl
label values Abroad Adoptionl


*Categoria Base Country
tab Country AwarenessPV, row nofreq
tab Country AwarenessEV, row nofreq
tab Country AwarenessHYB, row nofreq
tab Country AwarenessAPP, row nofreq
tab Country AwarenessLED, row nofreq

*Summary

sum Age

tab Education
tab SocialClass
tab Woman
tab Friendliness
tab NonRenewable
tab Country
tab Ownership
tab Abroad

*Base Country => Paraguay (600) o Republica Dominicana (214)
*Elegi Paraguay porque en tiende a estar mas al medio en todos los rankings y sin Colombia es la Mediana.

*Awareness

logit AwarenessLED Woman Age Saving Friendliness NonRenewable i.Education ib5.SocialClass Abroad ib600.Country
estimates store AwLED
lroc, nograph
estat classification, cutoff(.6877)
margins, dydx(*)

logit AwarenessPV Woman Age Saving Friendliness NonRenewable i.Education ib5.SocialClass Abroad ib600.Country
estimates store AwPV
lroc, nograph
estat classification, cutoff(.5077)
*margins Country Woman, dydx(Woman) at(Education=3 SocialClass=3 Friendliness=0 NonRenewable=0 Saving=0)
*margins Country Woman, dydx(Woman) at(Education=1 SocialClass=5 Friendliness=0 NonRenewable=0 Saving=0)
*margins Country Woman, dydx(Age) at(Education=1 SocialClass=5 Friendliness=0 NonRenewable=0 Saving=0 Woman=0)
*margins Country, dydx(Saving) at(Education=3 SocialClass=3 Friendliness=0 NonRenewable=0 Woman=0)
*margins, dydx(Woman Saving Friendliness)
*marginsplot, recast(bar)
margins, dydx(*)

logit AwarenessAPP Woman Age Saving Friendliness NonRenewable i.Education ib5.SocialClass Abroad ib600.Country
estimates store AwAPP
lroc, nograph
estat classification, cutoff(.2389)
margins, dydx(*)

logit AwarenessEV Woman Age Saving Friendliness NonRenewable i.Education ib5.SocialClass Abroad ib600.Country
estimates store AwEV
lroc, nograph
estat classification, cutoff(.4374)
margins, dydx(*)

logit AwarenessHYB Woman Age Saving Friendliness NonRenewable i.Education ib5.SocialClass Abroad ib600.Country
estimates store AwHYB
lroc, nograph
estat classification, cutoff(0.2316)
margins, dydx(*)

outreg2 [AwPV AwEV AwHYB AwAPP AwLED] using Table1.rtf, replace word dec(4) drop(i.Country 32.Country)
outreg2 [AwPV AwEV AwHYB AwAPP AwLED] using TableA.rtf, eform cti(odds ratio) replace word dec(4)
outreg2 [AwPV AwEV AwHYB AwAPP AwLED] using TableB.rtf, replace word dec(4) drop(i.Education i.SocialClass Age Woman Friendliness NonRenewable Saving 1.SocialClass) nocon

*Adoption
*Eliminar Ecuador por exceso de missings
drop if AdoptionLED<0 | Country==218

heckprob AdoptionLED Woman Age Saving Friendliness NonRenewable i.Education ib5.SocialClass Ownership ib600.Country , select(AwarenessLED = Woman Age Saving Friendliness NonRenewable i.Education ib5.SocialClass Abroad ib600.Country)
outreg2 Education SocialClass Age Woman Friendliness NonRenewable Saving Ownership using Table2.rtf, replace word dec(4) drop(i.Country 32.Country)

predict y_hat
replace y_hat = 0 if y_hat < 0.4012
replace y_hat = 1 if y_hat >= 0
gen OK = 1 if AdoptionLED == y_hat
replace OK = 0 if OK == .
mean(OK)

tab AdoptionLED y_hat
