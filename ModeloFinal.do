set more off
clear
cls

*Carga de datos

insheet using "C:\Users\Martin\Documents\Posdoc\Latinobarometro2018\Datos.csv"
drop if s10 < 0 
drop if s1  < 0
drop if edad < 0
drop if sexo < 0
drop if p38ni < 0
drop if p62nf < 0
drop if s21b < 0

*Variables dependientes
gen AwarenessPV = p34niac
gen AwarenessEV = p34niab
gen AwarenessHYB = p34niae
gen AwarenessLED = p34niaa
gen AwarenessAPP = p34niad

gen AdoptionLED = p34niba

*variables descriptivas del individuo, incluye Age y sexo
gen Woman = sexo
gen Age = edad
gen Country = idenpa

*Educacion: 5 categorias (1 baja escolaridad - alta escolaridad)
gen Education = s10

*Viajo al exterior alguna vez

*Ingreso: 5 categorias (5 muy bajo - 1 muy alto)
gen SocialClass = s1

*Llevo/a adelante medidas de ahorro energetico 
gen Saving = p38ni

*Amigabilidad con nuevas tecnologias: cree que los niños deben aprender sobre nuevas tecnologias?
gen Friendliness = p62nf

*Tiene casa propia
gen Ownership = s21b

*Percepcion del individuo respecto a las 2 principales fuentes de energia consumidas por su pais: NonRenewable o no NonRenewable
gen NonRenewable = 1 if (p29nid==1) | (p29nie==1) | (p29nif==1)
replace NonRenewable = 0 if NonRenewable ==.

gen SocialNet=1 if s12ma==1 | s12mb==1 | s12mc==1 | s12md==1 | s12mf==1 | s12mg==1 | s12mh==1 
replace SocialNet=0 if SocialNet==.

recode AwarenessPV (2=0)
recode AwarenessEV (2=0)
recode AwarenessHYB (2=0)
recode AwarenessLED (2=0)
recode AwarenessAPP (2=0)

recode AdoptionLED (2=0)

recode Woman (2=1) (1=0) 
recode Education (2/7=2) (8/13=3) (14/17=4) 
recode Saving (1=0)
recode Saving (2/4=1)
recode Friendliness (2=1)
recode Friendliness (3/4=0)
recode Ownership (2=0)

label define Educationl    1 "No Edu" 2 "Edu <= 7" 3 "Edu <=13" 4 "Edu > 14"
label define Awarenessl 1 "Knows" 0 "Not Knows"
label define Adoptionl 1 "Yes" 0 "No"

label values Education Educationl  
label values AwarenessPV Awarenessl
label values AwarenessEV Awarenessl
label values AwarenessHYB Awarenessl
label values AwarenessAPP Awarenessl
label values AwarenessLED Awarenessl
label values AdoptionLED Adoptionl
label values Saving Adoptionl
label values Friendliness Adoptionl
label values Ownership Adoptionl

*Categoria Base Country
tab Country AwarenessPV, row nofreq
tab Country AwarenessEV, row nofreq
tab Country AwarenessHYB, row nofreq
tab Country AwarenessAPP, row nofreq
tab Country AwarenessLED, row nofreq

*Summary

sum Age

tab Education
tab SocialClass
tab Woman
tab Friendliness
tab NonRenewable
tab Country
tab Ownership

*Base Country => Paraguay (600) o Republica Dominicana (214)
*Elegi Paraguay porque en tiende a estar mas al medio en todos los rankings y sin Colombia es la Mediana.

*Awareness

logit AwarenessLED Woman Age Saving Friendliness NonRenewable SocialNet i.Education ib5.SocialClass ib600.Country if (AwarenessLED >= 0) & !(AwarenessLED==0 & AdoptionLED==1)
estimates store AwLED
lroc, nograph
estat classification, cutoff(.6859)

logit AwarenessPV Woman Age Saving Friendliness NonRenewable SocialNet i.Education ib5.SocialClass ib600.Country if (AwarenessPV >= 0)
estimates store AwPV
lroc, nograph
estat classification, cutoff(.5042)

logit AwarenessAPP Woman Age Saving Friendliness NonRenewable SocialNet i.Education ib5.SocialClass ib600.Country if (AwarenessAPP >= 0)
estimates store AwAPP
lroc, nograph
estat classification, cutoff(.2370)

logit AwarenessEV Woman Age Saving Friendliness NonRenewable SocialNet i.Education ib5.SocialClass ib600.Country if (AwarenessEV >= 0)
estimates store AwEV
lroc, nograph
estat classification, cutoff(.4353)

logit AwarenessHYB Woman Age Saving Friendliness NonRenewable SocialNet i.Education ib5.SocialClass ib600.Country if (AwarenessHYB >= 0)
estimates store AwHYB
lroc, nograph
estat classification, cutoff(0.2298)

outreg2 [AwLED AwPV AwAPP AwEV AwHYB] using Table1.rtf, replace word dec(4) drop(i.Country 32.Country) e(r2_p ll)
outreg2 [AwLED AwPV AwAPP AwEV AwHYB] using TableA.rtf, eform cti(odds ratio) replace word dec(4) nose
outreg2 [AwLED AwPV AwAPP AwEV AwHYB] using TableB.rtf, replace word dec(4) drop(i.Education i.SocialClass Age Woman Friendliness NonRenewable Saving 1.SocialClass) nocon e(r2_p ll)

*margins Country Woman, dydx(Woman) at(Education=3 SocialClass=3 Friendliness=0 NonRenewable=0 Saving=0)
*margins Country Woman, dydx(Woman) at(Education=1 SocialClass=5 Friendliness=0 NonRenewable=0 Saving=0)
*margins Country Woman, dydx(Age) at(Education=1 SocialClass=5 Friendliness=0 NonRenewable=0 Saving=0 Woman=0)
*margins Country, dydx(Saving) at(Education=3 SocialClass=3 Friendliness=0 NonRenewable=0 Woman=0)
*margins, dydx(Woman Saving Friendliness)
*marginsplot, recast(bar)

*Adoption
*Eliminar Ecuador por exceso de missings
drop if AdoptionLED<0 | Country==218 | AwarenessLED==0
tab AdoptionLED if (AdoptionLED>=0) & (AwarenessLED==1) & !(Country==218)

logit AdoptionLED Woman Age Saving Friendliness NonRenewable SocialNet i.Education ib5.SocialClass Ownership ib600.Country if (AdoptionLED>=0) & (AwarenessLED==1) & !(Country==218)

outreg2 using Table2.rtf, replace word dec(4) drop(i.Country 32.Country) e(r2_p ll)
outreg2 using Table2.rtf, append word dec(4) eform cti(odds ratio) drop(i.Country 32.Country) nose noaster
outreg2 using TableD.rtf, replace word dec(4) drop(i.Education i.SocialClass Age Woman Friendliness Ownership NonRenewable Saving 1.SocialClass) nocon e(r2_p ll)
outreg2 using TableD.rtf, eform cti(odds ratio) append word dec(4) drop(i.Education i.SocialClass Age Woman Friendliness Ownership NonRenewable Saving 1.SocialClass) nocon nose noaster

lroc, nograph
estat classification, cutoff(0.8316)

/* Evaluacion del sesgo
quietly tabulate Education, generate(Education)
quietly tabulate SocialClass, generate(SocialClass)
quietly tabulate Country, generate(Country)
logit AdoptionLED Woman Age Saving Friendliness NonRenewable SocialNet Education2 Education3 Education4 SocialClass1 SocialClass2 SocialClass3 SocialClass4 Ownership Country1 Country2 Country3 Country4 Country5 Country6 Country7 Country9 Country10 Country11 Country12 Country13 Country15 Country16 Country17 if (AdoptionLED>=0) & (AwarenessLED==1) & !(Country==218)
estimates store log1
outreg2 using Comp.rtf, replace word dec(5)
relogit AdoptionLED Woman Age Saving Friendliness NonRenewable SocialNet Education2 Education3 Education4 SocialClass1 SocialClass2 SocialClass3 SocialClass4 Ownership Country1 Country2 Country3 Country4 Country5 Country6 Country7 Country9 Country10 Country11 Country12 Country13 Country15 Country16 Country17 if (AdoptionLED>=0) & (AwarenessLED==1) & !(Country==218)
outreg2 using Comp.rtf, append word dec(5)
estimates store log2
firthlogit AdoptionLED Woman Age Saving Friendliness NonRenewable SocialNet Education2 Education3 Education4 SocialClass1 SocialClass2 SocialClass3 SocialClass4 Ownership Country1 Country2 Country3 Country4 Country5 Country6 Country7 Country9 Country10 Country11 Country12 Country13 Country15 Country16 Country17 if (AdoptionLED>=0) & (AwarenessLED==1) & !(Country==218)
outreg2 using Comp.rtf, append word dec(5)
estimates store log3
hausman log2 log1, equation(1:1)
hausman log1 log3, equation(1:1)
*/
