set more off
clear

insheet using "C:\Users\Martin\Documents\Posdoc\Latinobarometro2018\Datos.csv"
drop if s10 < 0 
drop if p34niaa < 0
drop if p34niab < 0
drop if p34niac < 0
drop if p34niad < 0
drop if p34niae < 0
drop if p34nica < 0
drop if p34nicb < 0
drop if p34nicc < 0
drop if p34nicd < 0
drop if p34nice < 0
drop if p34niba < 0
drop if s1  < 0
drop if s13c < 0
drop if edad < 0
drop if sexo < 0
drop if p60nd < 0

*Variables dependientes
gen AwarenessPV = p34niac
gen AwarenessEV = p34niab
gen AwarenessHYB = p34niae
gen AwarenessLED = p34niaa
gen AwarenessAPP = p34niad

gen WillingnessPV = p34nicc
gen WillingnessEV = p34nicb
gen WillingnessHYB = p34nice
gen WillingnessLED = p34nica
gen WillingnessAPP = p34nicd

gen AdoptionPV = p34nibc
gen AdoptionEV = p34nibb
gen AdoptionHYB = p34nibe
gen AdoptionLED = p34niba
gen AdoptionAPP = p34nibd

*variables descriptivas del individuo, incluye Age y sexo
gen Education    = s10
gen Woman = sexo
gen Age = edad
gen Income = s1

*Variables de comportamiento con respecto al aprendizaje
gen Abroad = s13c
gen LNT = p60nd

recode AwarenessPV (2=0) (1=1)
recode AwarenessEV (2=0) (1=1)
recode AwarenessHYB (2=0) (1=1)
recode AwarenessLED (2=0) (1=1)
recode AwarenessAPP (2=0) (1=1)

recode WillingnessPV (2=0) (1=1)
recode WillingnessEV (2=0) (1=1)
recode WillingnessHYB (2=0) (1=1)
recode WillingnessLED (2=0) (1=1)
recode WillingnessAPP (2=0) (1=1)

recode AdoptionPV (2=0) (1=1)
recode AdoptionEV (2=0) (1=1)
recode AdoptionHYB (2=0) (1=1)
recode AdoptionLED (2=0) (1=1)
recode AdoptionAPP (2=0) (1=1)

recode Woman (2=1) (1=0) 

recode Education (2/13=2) (14=15) 
recode Education (1=1) (15=4) (16=3) (17=3)
recode Abroad (1=0)
recode Abroad (2/4=1)

label define Educationl    1 "No Edu" 2 "Incomplete Compulsory Education" 3 "H.School and Vocational" 4 "University"
label define Awarenessl 1 "Knows" 0 "Not Knows"
label define Willingnessl 1 "Yes" 0 "No"
label define Abroadl 1 "Abroad" 0 "Not abroad"

label values Education Educationl  
label values AwarenessPV Awarenessl
label values AwarenessEV Awarenessl
label values AwarenessHYB Awarenessl
label values AwarenessAPP Awarenessl
label values AwarenessLED Awarenessl
label values WillingnessPV Willingnessl
label values WillingnessEV Willingnessl
label values WillingnessHYB Willingnessl
label values WillingnessAPP Willingnessl
label values WillingnessLED Willingnessl
label values AdoptionPV Willingnessl
label values AdoptionEV Willingnessl
label values AdoptionHYB Willingnessl
label values AdoptionAPP Willingnessl
label values AdoptionLED Willingnessl
label values Abroad Abroadl

*Technology awareness

logit AwarenessPV i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)

logit AwarenessEV i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)

logit AwarenessHYB i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)

logit AwarenessAPP i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)

logit AwarenessLED i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)

*Willingness to adopt a new technology

logit WillingnessPV i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)

logit WillingnessEV i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)

logit WillingnessHYB i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)

logit WillingnessAPP i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)

logit WillingnessLED i.Education ib5.Income Age Woman i.Country Abroad LNT
lroc, nograph
estat classification
estat ic
margins, dydx(*)
margins, dydx(Education) over(Income)


