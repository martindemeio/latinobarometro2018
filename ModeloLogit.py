# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal.
"""

import numpy as np
import pandas as pd
import statsmodels.api as sm
from sklearn.metrics import roc_auc_score
from sklearn.metrics import classification_report

Datos = pd.read_excel('DatosLimpios.xls', sheet_name='Sheet1')

Education = pd.get_dummies(Datos.iloc[:,5],prefix="Education")
Gender = pd.get_dummies(Datos.iloc[:,10],prefix="Gender")
Social = pd.get_dummies(Datos.iloc[:,12],prefix="Social")
Country = pd.get_dummies(Datos.iloc[:,11],prefix="Country")
Age = Datos.iloc[:,9]
LNT = pd.get_dummies(Datos.iloc[:,8],prefix="LNT")
Abroad = pd.get_dummies(Datos.iloc[:,7],prefix="Abroad")
AwaPV = Datos.iloc[:,0]
AwaEV = Datos.iloc[:,1]
AwaHYB = Datos.iloc[:,2]
AwaLED = Datos.iloc[:,3]
AwaAPP = Datos.iloc[:,4]

Input = pd.concat([Education.iloc[:,1:], Gender.iloc[:,1:], Social.iloc[:,1:], LNT.iloc[:,1:], Abroad.iloc[:,1:], Country.iloc[:,1:], Age] , axis=1)
Input = sm.add_constant(Input)

Output = AwaPV

logit_model=sm.Logit(Output,Input)
result=logit_model.fit()
print(result.summary2())
print("Bic " + str(result.bic))
margins = result.get_margeff(dummy=True)
print(margins.summary())

pred = result.predict(Input)

pred[pred > 0.5] = 1
pred[pred <= 0.5] = 0
print("Accuracy " + str((Output == pred).mean()))

print(classification_report(Output,pred))
print("ROC " + str(roc_auc_score(Output,pred)))