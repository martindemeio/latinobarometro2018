set more off
clear

*Carga de datos

insheet using "C:\Users\Martin\Documents\Posdoc\Latinobarometro2018\Datos.csv"
drop if idenpa==218
drop if s10 < 0 
drop if p34niaa < 0
drop if p34niab < 0
drop if p34niac < 0
drop if p34niad < 0
drop if p34niae < 0
drop if p34niba < 0
drop if s1  < 0
drop if s13c < 0
drop if edad < 0
drop if sexo < 0
*drop if p60nd < 0
drop if p38ni < 0
drop if p62nf < 0

*Variables dependientes
gen AwarenessPV = p34niac
gen AwarenessEV = p34niab
gen AwarenessHYB = p34niae
gen AwarenessLED = p34niaa
gen AwarenessAPP = p34niad

gen AdoptionLED = p34niba

*variables descriptivas del individuo, incluye Age y sexo
gen Education    = s10
gen Woman = sexo
gen Age = edad
gen Income = s1
gen Saving = p38ni

*Variables de comportamiento con respecto al aprendizaje de nuevas tecnologias
gen Abroad = s13c
*gen Friendliness = p60nd
gen Friendliness = p62nf
gen Country = idenpa

recode AwarenessPV (2=0) (1=1)
recode AwarenessEV (2=0) (1=1)
recode AwarenessHYB (2=0) (1=1)
recode AwarenessLED (2=0) (1=1)
recode AwarenessAPP (2=0) (1=1)

recode AdoptionLED (2=0) (1=1)

recode Woman (2=1) (1=0) 
recode Education (2/13=2) (14=15) 
recode Education (1=1) (15=4) (16=3) (17=3)
recode Abroad (1=0)
recode Abroad (2/4=1)
recode Saving (1=0)
recode Saving (2/4=1)
recode Friendliness (2=1)
recode Friendliness (3/4=0)

label define Educationl    1 "No Edu" 2 "Incomplete Compulsory Education" 3 "H.School and Vocational" 4 "University"
label define Awarenessl 1 "Knows" 0 "Not Knows"
label define Adoptionl 1 "Yes" 0 "No"
label define Abroadl 1 "Abroad" 0 "Not abroad"

label values Education Educationl  
label values AwarenessPV Awarenessl
label values AwarenessEV Awarenessl
label values AwarenessHYB Awarenessl
label values AwarenessAPP Awarenessl
label values AwarenessLED Awarenessl
label values AdoptionLED Adoptionl
label values Abroad Abroadl
label values Saving Adoptionl
label values Friendliness Adoptionl

*Technology awareness

probit AwarenessPV i.Education ib5.Income Age Woman i.Country Abroad Friendliness
lroc, nograph
estat classification
margins, dydx(*)
margins, eyex(Age)
margins, eyex(Woman)

probit AwarenessEV i.Education ib5.Income Age Woman i.Country Abroad Friendliness
lroc, nograph
estat classification
margins, dydx(*)
margins, eyex(Age)
margins, eyex(Woman)

probit AwarenessHYB i.Education ib5.Income Age Woman i.Country Abroad Friendliness
lroc, nograph
estat classification
margins, dydx(*)
margins, eyex(Age)
margins, eyex(Woman)

probit AwarenessAPP i.Education ib5.Income Age Woman i.Country Abroad Friendliness
lroc, nograph
estat classification
margins, dydx(*)
margins, eyex(Age)
margins, eyex(Woman)

probit AwarenessLED i.Education ib5.Income Age Woman i.Country Abroad Friendliness
lroc, nograph
estat classification
margins, dydx(*)
margins, eyex(Age)
margins, eyex(Woman)

heckprob AdoptionLED i.Education ib5.Income Age Woman i.Country Friendliness Saving, select(AwarenessLED = i.Education ib5.Income Age Woman i.Country Abroad Friendliness)
predict y_hat
margins, dydx(*)
margins, eyex(Age)
margins, eyex(Woman)

replace y_hat = 0 if y_hat < 0.5
replace y_hat = 1 if y_hat > 0
gen OK = 1 if AdoptionLED == y_hat
replace OK = 0 if OK == .
mean(OK)

biprobit (AwarenessLED=i.Education ib5.Income Age Woman i.Country Saving Friendliness) (AdoptionLED=i.Education ib5.Income Age Woman i.Country Abroad Friendliness), noskip
predict y_hat2
margins, dydx(*)
margins, eyex(Age)
margins, eyex(Woman)

replace y_hat2 = 0 if y_hat2 < 0.5
replace y_hat2 = 1 if y_hat2 > 0
gen OK2 = 1 if AdoptionLED == y_hat2
replace OK2 = 0 if OK2 == .
mean(OK2)
